# Shared Artifacts

In this template we are leveraging the artifacts feature. Our job has three
stages and each stage is using a different docker image, while sharing some data.

1. We get some BIDS data with datalad and store it in a folder called `data/`
2. We validate the dataset with bids-validator on the `data/` folder from the stage above and write report.json to a folder called `report/`
3. We use the json file in `report/` to generate a very basic HTML file and put the result in a folder called `public/`

Because we named the third stage `pages`, the content of the public folder will be served as a website on https://citemplates.mpib.berlin/shared_artifacts.

Cool stuff.
